import { createStore, combineReducers, compose } from "redux";

import chatReducer from "./reducers/chat";

const rootReducer = combineReducers({
  chat: chatReducer,
});

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers());

export default store;

import { makeStyles } from "@material-ui/core/styles";
import { defaultPadding } from "..";

const footerStyle = {
  root: {
    textAlign: "center",
    fontWeight: "bold",
    padding: defaultPadding,
  },
};

export default makeStyles(footerStyle);

import { makeStyles } from "@material-ui/core/styles";
import { primary } from "..";

const controlsStyle = {
  root: {
    marginTop: "1.5em",
  },
  underline: {
    "&:after": {
      borderBottom: `2px solid ${primary[1]}`,
    },
  },
  focused: {
    color: `${primary[1]} !important`,
  },
  outlinedPrimary: {
    color: `${primary[1]} !important`,
    borderColor: "rgb(0,145,199, 0.5) !important",
  },
};

export default makeStyles(controlsStyle);

import { makeStyles } from "@material-ui/core/styles";
import { white, gray } from "..";

const likeStyle = {
  root: {
    position: "absolute",
    top: "-12px",
    border: `2px solid ${white}`,
    right: "-12px",
    backgroundColor: white,
    borderRadius: "50px",
    fontSize: "24px",
    "&:hover": {
      fill: gray[0],
    },
  },
};

export default makeStyles(likeStyle);

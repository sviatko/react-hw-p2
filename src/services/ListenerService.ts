export const upArrowKeyboardClick = (callback: any) => {
  document.addEventListener("keyup", (e) => {
    const key = e.key;
    if (key === "ArrowUp") callback();
  });
};

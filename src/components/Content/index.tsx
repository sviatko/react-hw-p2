import React, { useRef, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Card, CardContent, CircularProgress } from "@material-ui/core";
import { Dispatch } from "redux";
import moment from "moment";

import useStyles from "../../assets/jss/components/contentStyle";
import Message from "../Message";
import Divider from "../Divider";
import { IChatState } from "../../interfaces/IChatState";
import { IMessage } from "../../interfaces/IMessage";
import { me, groupByDay } from "../../services/MessageService";
import * as actionCreator from "../../store/actions";

type Props = {
  messages: Array<IMessage>;
  setMessages: (messages: Array<IMessage>) => void;
  editMessage: (message: IMessage) => void;
  deleteMessage: (message: IMessage) => void;
};

const Content = ({
  messages,
  setMessages,
  editMessage,
  deleteMessage,
}: Props) => {
  let scrollToDiv = useRef<HTMLDivElement>(null);

  const [isLoading, setLoading] = useState(true);
  const classes = useStyles();
  const myMessage = me(messages);
  const tooltips = {
    like: "Double click to toggle like",
    settings: "Double click to delete",
  };

  const editMessageHandler = (message: IMessage) => {
    editMessage(message);
  };

  const removeMessageHander = (message: IMessage) => {
    if (window.confirm("Are you sure, you want to delete the message?")) {
      deleteMessage(message);
    }
  };

  useEffect(() => {
    if (scrollToDiv.current) {
      scrollToDiv.current.scrollIntoView({
        block: "center",
        behavior: "smooth",
      });
    }
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, [messages, setMessages]);

  let content: any = [];
  const groupedMessages = groupByDay(messages);
  if (groupedMessages) {
    content = [];
    const keys = Object.keys(groupedMessages);

    keys.map((day) => {
      if (groupedMessages) {
        content.push(<Divider text={day} />);
        groupedMessages[day].map((message, index) => {
          content.push(
            <Message
              floatRight={myMessage?.userId === message.userId}
              key={`message-${index}`}
              text={message.text}
              avatartUrl={message.avatar}
              time={moment(message.createdAt).format("H:m")}
              tooltip={
                myMessage?.userId === message.userId
                  ? tooltips.settings
                  : tooltips.like
              }
              removeMessageHander={() => removeMessageHander(message)}
              onClick={() => editMessageHandler(message)}
            />
          );
          content.push(<div ref={scrollToDiv}></div>);

          return;
        });
      }

      return;
    });
  }

  return (
    <Card className={classes.root}>
      <CardContent className={isLoading ? classes.textCenter : ""}>
        {isLoading && <CircularProgress />}
        {content && !isLoading && content.map((item: any) => item)}
      </CardContent>
    </Card>
  );
};

const mapStateToProps = (state: { chat: IChatState }) => {
  return {
    messages: state.chat.messages,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setMessages: (messages: Array<IMessage>) =>
      dispatch(actionCreator.setMessages(messages)),
    editMessage: (message: IMessage) =>
      dispatch(actionCreator.editMessage(message)),
    deleteMessage: (message: IMessage) =>
      dispatch(actionCreator.deleteMessage(message)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);

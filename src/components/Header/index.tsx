import React from "react";
import PropTypes from "prop-types";

import { Card, CardContent } from "@material-ui/core";

import useStyles from "../../assets/jss/components/headerStyle";

type Props = {
  title: string;
  participantsTotal: number;
  messagesTotal: number;
  lastMessageAt: string;
};

const Header = ({
  title,
  participantsTotal,
  messagesTotal,
  lastMessageAt,
}: Props) => {
  const classes = useStyles();
  return (
    <Card>
      <CardContent className={classes.root}>
        <div className={classes.details}>
          <span className={classes.title}>{title}</span>
          <span>Participants: {participantsTotal}</span>
          <span>Messages: {messagesTotal}</span>
        </div>
        <span className={classes.lastMessage}>
          Last message at: {lastMessageAt}
        </span>
      </CardContent>
    </Card>
    // <header className={classes.root}>
    // <div className={classes.details}>
    //   <span className={classes.title}>{title}</span>
    //   <span>Participants: {participantsTotal}</span>
    //   <span>Messages: {messagesTotal}</span>
    // </div>
    // <span className={classes.lastMessage}>
    //   Last message at: {lastMessageAt}
    // </span>
    // </header>
  );
};

export default Header;

Header.prototype = {
  title: PropTypes.string.isRequired,
  lastMessageAt: PropTypes.string.isRequired,
  participantsTotal: PropTypes.number.isRequired,
  messagesTotal: PropTypes.number.isRequired,
};

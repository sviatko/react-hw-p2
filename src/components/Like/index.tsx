import React from "react";
import { Favorite } from "@material-ui/icons";

import useStyles from "../../assets/jss/components/likeStyle";

const Like = () => <Favorite className={useStyles().root} />;

export default Like;

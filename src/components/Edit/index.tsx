import React from "react";
import { Edit as EditIcon } from "@material-ui/icons";

import useStyles from "../../assets/jss/components/editStyle";

type Props = { onClick?: any };

const Edit = ({ onClick }: Props) => (
  <span onClick={onClick}>
    <EditIcon className={useStyles().root} />
  </span>
);

export default Edit;

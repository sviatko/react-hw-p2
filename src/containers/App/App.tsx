import React from "react";

import Logo from "../../components/Logo";
import Chat from "../../components/Chat";
import Footer from "../../components/Footer";

import useStyles from "../../assets/jss/components/appStyle";

const App = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div>
        <Logo />
        <Chat />
      </div>
      <Footer />
    </div>
  );
};

export default App;
